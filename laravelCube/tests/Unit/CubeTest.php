<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Cube;

class CubeSummationTest extends TestCase
{
    /**
     * Auxiliary function to execute the 4 cases found at hackerrank
     *
     * @return void
     */
    private function testCubeClass($InInputfile, $InOutputFile){
        $_fp = fopen($InInputfile, "r");
        $outputFile = fopen($InOutputFile, "r");
        $T = fgets($_fp);
        $cube = [];
        if (1 <= $T && $T <= 50) {
            $message = "";
            for($it = 0; $it < $T; $it++) {
                $configLine = fgets($_fp);
                list($N, $M) = preg_split("/[\s]+/", $configLine);

                if ((1 <= $N && $N <= 100) && (1 <= $M && $M <= 1000)) {
                    $cube = new Cube($N);
                    for($j = 0; $j < $M; $j++) {
                        $line   = "";
                        $line   = fgets($_fp);
                        $cubeOutput = $cube->command($line);
                        if($cubeOutput != null){
                            $output = strtok(fgets($outputFile), "\n");
                            $this->assertEquals($output,$cubeOutput);
                        }
                    }
                }
            }
        }
    }

    /**
     * function to execute the zero case
     *
     * @return void
     */
    public function testCubeClass_0()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input0.txt",
                                "./tests/Unit/output0.txt");
    }


    /**
     * function to execute the first case
     *
     * @return void
     */
    public function testCubeClass_1()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input1.txt",
                                "./tests/Unit/output1.txt");
    }

    /**
     * function to execute the second case
     *
     * @return void
     */
    public function testCubeClass_2()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input2.txt",
                                "./tests/Unit/output2.txt");
    }

    /**
     * function to execute the third case
     *
     * @return void
     */
    public function testCubeClass_3()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input3.txt",
                                "./tests/Unit/output3.txt");
    }

    /**
     * function to execute the fourth case
     *
     * @return void
     */
    public function testCubeClass_4()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input4.txt",
                                "./tests/Unit/output4.txt");
    }

    /**
     * function to execute the fifth case
     *
     * @return void
     */
    public function testCubeClass_5()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input5.txt",
                                "./tests/Unit/output5.txt");
    }

    /**
     * function to execute the sixth case
     *
     * @return void
     */
    public function testCubeClass_6()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input6.txt",
                                "./tests/Unit/output6.txt");
    }

    /**
     * function to execute the seventh case
     *
     * @return void
     */
    public function testCubeClass_7()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input7.txt",
                                "./tests/Unit/output7.txt");
    }

    /**
     * function to execute the eighth case
     *
     * @return void
     */
    public function testCubeClass_8()
    {
        ini_set('memory_limit', '-1');
        $this->testCubeClass("./tests/Unit/input8.txt",
                                "./tests/Unit/output8.txt");
    }
}
