<?php

/**
* Cube
*
* @author: fabiocasmar
*
* @description:
* Controller for the home view
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Cube;

class CubeController extends Controller
{
    public function index()
    {
        return view('home.home');
    }

}