<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use App\Cube;

/**
* Cube
*
* @author: fabiocasmar
*
* @description:
* Controller for upload the file and show the results
*/

class UploadController extends Controller {

    public function index(Request $request){
        $output = [];
        $file = $request->file('file')->openFile();
        $T = $file->fgets();
        if (1 <= $T && $T <= 50) {
            $message = "";
            for($it = 0; $it < $T; $it++) {
                $configLine = $file->fgets();
                list($N, $M) = preg_split("/[\s]+/", $configLine);

                if ((1 <= $N && $N <= 100) && (1 <= $M && $M <= 1000)) {
                    $cube = new Cube($N);
                    for($j = 0; $j < $M; $j++) {
                        $line = "";
                        $line = $file->fgets();
                        array_push($output, $cube->command($line));
                    }
                }
            }
        }
        return view('home.output', ['output' => $output]);
    }
}