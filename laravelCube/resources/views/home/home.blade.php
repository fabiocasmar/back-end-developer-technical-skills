@extends('layouts.master')

@section('content')

    <form action="{{ URL::to('output') }}" method="post" enctype="multipart/form-data">
    	<div class="col-md-10">
            <div class="form-group">
		        <label>Seleccione el caso de prueba a subir:</label>
                <input type="file" class="file" name="file" id="file">
		        <input type="hidden" value="{{ csrf_token() }}" name="_token">
		    </div>
		</div>
    </form>

@endsection