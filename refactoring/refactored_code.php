<?php public 


/**
* post_confirm
*
* @author: fabiocasmar
*
* @description:
*  Function to control the post confirmation
*/
function post_confirm(){
    // Initialize variables and and find the service and the driver
    $id = Input::get('service_id');
    $driver_id = Input::get('driver_id');
    $servicio = Service::find($id);

    // Error validations returns
    if($servicio == NULL){
        return Response::json(array('error' => 3));
    }
    if($servicio->status_id == '6'){
        return Response::json(array('error' => 2));
    }
    if(!($servicio->driver_id == NULL && $servicio->status_id == '1')){
        return Response::json(array('error' => 1));
    }
    if($servicio->user->uuid == ''){
        return Response::json(array('error' => 0));
    }
    
    // Update service status, driver status and update service car.
    $driverTmp = Driver::find($driver_id);
    $servicio = Service::update($id, 
        array(  'driver_id' => $driver_id,
                'status_id'   => '2'
            ));
    Drive::update($driver_id, array('available' => '0'));
    Service::update($id, array('car_id' => $driverTmp->car_id));

    // Send push notificaction
    $pushMessage = 'TU servicio ha sido confirmado';
    $push = Push::make();
    if($servicio->user->type == '1'){
        $result = $push->ios($servicio->user->uuid, $pushMessage), 
                            1, 'honk.wav', 'Open', 
                                array('serviceId' => $servicio->id));
    }else{
        $result = $push->android2($servicio->user->uuid, $pushMessage),
                            1, 'honk.wav', 'Open', 
                                array('serviceId' => $servicio->id));
        
    }
    return Response::json(array('message' => $result));
}

?>