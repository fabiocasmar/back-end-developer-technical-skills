<?php

    require_once __DIR__."/cube.php";

    $_fp = fopen("php://stdin", "r");
    
    $T = fgets($_fp);


    $cube = [];

    if (1 <= $T && $T <= 50) {
        $message = "";
        for($it = 0; $it < $T; $it++) {
            $configLine = fgets($_fp);
            list($N, $M) = preg_split("/[\s]+/", $configLine);

            if ((1 <= $N && $N <= 100) && (1 <= $M && $M <= 1000)) {
                $cube = new Cube($N);
                for($j = 0; $j < $M; $j++) {
                    $line = "";
                    $line = fgets($_fp);
                    echo $cube->command($line);
                }
            }
        }
    }

?>