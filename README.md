# BACK-END DEVELOPER TECHNICAL SKILLS #

El presente proyecto de **BitBucket** contiene las respuesta para la prueba técnica de Back-end de Rappi.

# 1 CODING CHALLENGE  (70 puntos) #
## 1.1 Enunciado ##
La primera parte de la prueba consiste en un Coding Challenge tomado de hackerrank. Lo puede encontrar en el siguiente link: https://www.hackerrank.com/challenges/cube-summation/problem .

Puede escribir el programa en el lenguaje de programación en el que se sienta más cómodo, aunque se prefiere el uso de los siguientes lenguajes: Java, Node, Kotlin, Scala, o PHP. Puede usar cualquier mecanismo para la entrada y salida de datos del programa. Una vez haya terminado el código describa en un documento brevemente:

1. Las capas de la aplicación (por ejemplo, capa de persistencia, vista, de aplicación, etc.) y qué clases pertenecen a cuál.
2. La responsabilidad de cada clase creada.

Recibirá puntos extra si:

1. El mecanismo usado para la entrada y salida de datos es Web (5 puntos).
2. Usa Bitbucket (preferido), git o SVN y la historia del programa muestra su progreso en el
desarrollo, usando commits con unidades de funcionalidad (5 puntos).
3. Usa pruebas unitarias y/o de validación (acceptance) que demuestren el buen
funcionamiento del programa (10 puntos).

## 1.2 Solución Algorítmica Desarrollada ##
	
El problema consiste en realizar una serie de actualizaciones y búsquedas en un cubo de 3 dimensiones de tamaño N. Para esto se utilizó **Binary Indexed Tree** (**BIT**). 

En la presente sección se explicará cómo funcionan a nivel general los **BIT**, y cómo se implementó para resolver el problema.

### 1.2.1 Binary Indexed Tree ###

**Binary Indexed Trees**, tambien conocidos como **Fenwick Trees**, son una estructura de datos desarrollada por Peter M. Fenwick, originalmente pensada para compresión de datos. Hoy en día es utilizada para almacenar frecuencias y manipular tablas de frecuencia acumuladas.

#### 1.2.1.1 Explicación general ####

Los **Binary Indexed Trees** parten de dos premisas:

1. Cada número entero puede ser representado como una potencia de dos.
2. La frecuencia acumulada se puede representar como la suma de conjuntos de subfrecuencias.

En nuestro caso, cada conjunto contiene un número sucesivo de frecuencias no solapadas.

Una forma de resolver el problema de CubeSummation seria recorriendo cada elemento del cubo entre los rangos, sin embargo, esto daría una complejidad algoritmica de **O(N^3)**. El **BIT** permite solucionar este problema de forma mas eficiente. Para solucionarlo, definiremos un árbol donde habran nodos responsables, y nodos dependientes.

Cada nodo responsable se encarga de mantener la suma de los valores de todos sus nodos dependientes. De esta manera, para calcular una suma en cualquier intervalo solo hay que tomar los valores en los nodos responsables que estén en el intervalo, y sumarlos, ya que estos comprimen en ellos la información de sus nodos dependientes.


Supongamos que se tiene un problema análogo a CubeSummation, pero de una sola dimensión, es decir un arreglo en vez de un cubo, cuyos valores son [2, 3, 1, 1, 5, 6, 0, 1]. En la siguiente figura podemos ver los elementos responsables y dependientes, y sus valores:

![arbol](./READMEimg/cube2.png)

#### 1.2.1.2 Obtener la suma acumulada ####

Dada una posición x1, podemos usar el árbol de Fewick para obtener la suma de los valores acumulados desde la posición x1 hasta la posición x2. Esto es A[x1] + A[x1+1] + … + A[x2]. La forma de hacer esto es comenzar en la posición x1, y tomar los valores de los elementos responsables del árbol hasta llegar a x2.


#### 1.2.1.3 Actualizar las cantidades ####

Si se desea actualizar una casilla con valor un a en la posición x1, debemos, cambiar el valor en posición x1 del arbol y notificar a todos los elementos que son responsables de el que ha ocurrido un cambio en el valor. Por ejemplo, si cambiamos un valor en la posición 2, tendríamos que notificar a la 4 y a la 8 de este cambio  ya que ambas son responsables por la 2. Esto mantiene el árbol sincronizado. La forma de hacerlo es actualizar el valor en la posición x1, y moverse hacia adelante en el árbol a todos los responsables haciéndoles saber que el elemento fue actualizado.

### 1.2.2  Solución: BIT en Cube Summation ###

El objetivo detrás de **BIT** consiste en afectar el mínimo número posible de nodos durante las operaciones de actualización y búsqueda, y la mejor forma de hacerlo es calculando el nodo hijo y el nodo padre de la forma más eficientemente posible, para así actualizar y obtener acumuladas eficientemente.

#### 1.2.2.1 Calculo del próximo ####

Para esto, en nuestra implementación calcularemos el padre de la siguiente manera:

1. Calculamos el complemento 2 del indice actual,
2. Realizamos un AND con el indice actual.
3. Restamos al indice actual.
En el código es representado como: ` $currentIndex -= ($currentIndex & -$currentIndex); `

El próximo hijo lo calcularemos de la siguiente manera:

1. Calculamos el complemento 2 del indice actual,
2. Realizamos un AND con el indice actual.
3. Sumamos al indice actual.
En el código es representado como: ` $currentIndex += ($currentIndex & -$currentIndex); `

#### 1.2.2.2 Suma de rangos ####

Los ejemplos presentados anteriormente fueron presentados para arreglos, sin embargo, esto es análogo para cubo, ya que son arreglos de 3 dimensiones. Matemáticamente podemos definir la suma entre los puntos (x1,y1,z1) y (x2,y2,z2) en nuestra representación del **BIT** como: 

```
∑(x1, y1, z1) hasta (x2, y2, z2) = sum(x2+1,y2+1,z2+1) 
                                        - sum(x1,y1,z1) 
                                          - sum(x1,y2+1,z2+1) 
                                            - sum(x2+1,y1,z2+1) 
                                              - sum(x2+1,y2+1,z1) 
                                                + sum(x1,y1,z2+1) 
                                                  + sum(x1,y2+1,z1) 
                                                    + sum(x2+1,y1,z1) 
```


Esto quiere decir que para realizar un query entre los puntos (1,1,1) hasta (4,4,3), tendriamos que calcular lo siguiente:

```
∑(1, 1, 1) hasta (4, 4, 3) = sum(5,5,4) - sum(1,1,1) - sum(1,5,4) 
                               - sum(5,1,4) - sum(5,5,3) + sum(1,1,4) 
                                 + sum(1,5,1) + sum(5,1,1)
```

Esto se puede apreciar visualmente en la imágen a continuación:
![∑(1, 1, 1) hasta (4, 4, 3)](./READMEimg/cube1.png)

El presente documento describe el contenido de la carpeta cube, la cual contiene dos Carpetas: hackRankCube y terminalCube.

Usando **BIT** la complejidad algoritmica es **O((log(n))^3)**, donde N es el tamaño del cubo.

## 1.3 Implementaciones de CubeSummation ##

Se realizaron 3 implementaciones de CubeSummation:

1. hackRankCube: una versión de CubeSummation adaptada para funcionar desde la web de HackerRank.
2. terminalCube: una versión de CubeSummation adaptada para funcionar desde el terminal
3. laravelCube: una versión de CubeSummation desarrollada con **Laravel y cuenta con pruebas funcionales**.

### 1.3.1 hackRankCube ###

#### 1.3.1.1 Archivos ####
1. hackRankCube/index.php

#### 1.3.1.2 ¿Cómo se ejecuta? ####

Basta con copiar y pegar el código en `https://www.hackerrank.com/challenges/cube-summation/problem`.

### 1.3.2 terminalCube ###

#### 1.3.2.1 ¿Cómo se ejecuta? ####
Para ejecutar un caso de prueba debe ejecutar el siguiente comando:  `php terminalCube/index.php < CASO.txt`, donde CASO.txt es el archivo del caso de prueba.

#### 1.3.2.2 Archivos ####
1. terminalCube/index.php: archivo que lee la entrada estandar y realiza el llamado al cube.php.
2. terminalCube/cube.php: realiza la implementación del cubo, así como la ejecución de los comandos.

### 1.3.3 LaravelCube ###

#### 1.3.3.1 Instalación ####

1.  ` cd back-end-developer-technical-skills `
2.  ` composer install `
3.  ` echo "export PATH=$PATH:$HOME/.composer/vendor/bin:./vendor/bin/" >> $HOME/.bash_profile `

#### 1.3.3.2 ¿Cómo iniciar? ####

1.  `php artisan serve` para iniciar la aplicación en http://localhost:8000/

#### 1.3.3.3 ¿Cómo funciona? ####

1. Al iniciar la aplicación, usted econtrará una vista similar a la siguiente imagen: ![Vista Inicial Web](./READMEimg/cap1.png)
2. Usted debe seleccionar el archivo que desea cargar: ![Cargar archivo](./READMEimg/cap2.png)
3. Luego se presentará un preview del archivo: ![Preview del archivo](./READMEimg/cap3.png)
4. El sistema procesará el caso de prueba y mostrará en pantalla el resultado:![Resultados del caso de Prueba](./READMEimg/cap4.png)


#### 1.3.3.4 Lógica ####

1. Se sube el archivo al servidor usando la interfaz grafica
2. Se envía a la vista output.
3. El controlador procesa la información del archivo usando un **BIT**
4. Se muestra en pantalla el resultado de dicho procesamiento.

#### 1.3.3.5 Archivos ####
En esta sección presentamos los archivos desarrollados así como una breve descripción de cada uno.
##### 1.3.3.5.1 Vistas #####
1. resources/views/layouts/master.blade.php: template que contiene el header de las vistas.
2. resources/views/home/home.blade.php: template para el home, permite cargar un archivo que luego será parseado.
3. resources/views/home/output.blade.php: template que parsea el archivo subido para luego mostrar el resultado por pantalla.

##### 1.3.3.5.2 Controladores #####
1. app/Http/Controllers/CubeController.php class CubeController: Controller de la vista que permite subir el archivo con el caso de prueba.
2. app/Http/Controllers/UploadController.php class UploadController: Controller de la vista que ejecuta el caso de prueba y muestra en pantalla el resultado.

##### 1.3.3.5.3 Clases #####
1. app/cube.php class Cube: clase encargada de manejar el cubo, e implementar su funcionamiento usando **BIT**.

##### 1.3.3.5.4 Pruebas #####
1. tests/Unit/CubeTest.php: contiene las pruebas unitarias a la clase Cube.

##### 1.3.3.5.5 Routes #####
1. routes/web.php: contiene las rutas a las vistas.

#### 1.3.3.6 Pruebas funcionales ####
Para ejecutar las pruebas unitarias usted sólo debe ejecutar el siguiente comando: `phpunit` .

## 1.4 Preguntas Teóricas 1 ##

### 1.4.1 Describa las capas de la aplicación (por ejemplo, capa de persistencia, vista, de aplicación, etc.) y qué clases pertenecen a cuál.  ###

En el proyecto presentado se muestra una arquitectura de Capas con Modelo-Vista-Controlador, el modelo de capas utilizado por Laravel, el framework utilizado para desarrollar la solución.

En la siguiente imagen podemos encontrar una explicación de las capas de la aplicación.
![Modelo-Vista-Controlador](./READMEimg/mvc1.jpg)

Todos los controllers pertecen a la capa Controlador, incluidos CubeController y  UploadController.

La clase Cube, pertenece a la capa Modelo.

### 1.4.2 La responsabilidad de cada clase creada.  ###

1. app/cube.php class Cube: clase encargada de manejar el cubo, e implementar su funcionamiento usando Binary Indexed Tree.
2. app/Http/Controllers/CubeController.php class CubeController: Controller de la vista que permite subir el archivo con el caso de prueba.
3. app/Http/Controllers/UploadController.php class UploadController: Controller de la vista que ejecuta el caso de prueba y muestra en pantalla el resultado.


# 2 CODE REFACTORING (20 puntos) #

## 2.1 Enunciado ##

El siguiente código muestra el método de un controlador que:

1. Recibe dos parámetros por POST: El id de un servicio, el id de un conductor.
2. Cambia el estado de varias entidades en la base de datos basado en la lógica del negocio.
3. Envía notificaciones y retorna una respuesta.

Refactorice y envíe el código y en un documento explique:

1. Las malas prácticas de programación que en su criterio son evidenciadas en el código.
2. Cómo su refactorización supera las malas prácticas de programación.


## 2.2 Implementaciones ##

A continuación se presenta el código antes de la refactorización:

```
<?php public function post_confirm(){
	$id = Input::get('service_id');
	$servicio = Service::find($id);
	//dd($servicio)
	if($servicio != NULL){
		if($servicio -> status_id == '6'){
			return Response::json(array('error' => '2'))
		}
		if($servicio -> driver_id == NULL && $servicio->status_id == '1'){
			$servicio = Service::update($id, array(
						'driver_id' => Input::get(driver_id),
						status_id   => '2'
							//Up Carro
							//,'pwd' => md5(Input::get('pwd'))

			));
			Drive::update(Input::get('driver_id'), array(
				"available" => '0'
			));
			$driverTmp = Driver::find(Input::get(driver_id));
			Service::update($id, array(
				'car_id' => $driverTmp->car_id
					//Up Carro
					//,'pwd' => md5(Input::get('pwd'))
			));
			//Notificar a usuario!!
			$pushMessage = 'TU servicio ha sido confirmado';
			/* $servicio = Service::find($id);
			  $push = Push::make();
			  if($servicio->user_>type == '1'){ //iPhone
			  $pushAns = $push->ios($servicio->user->uuid, $pushMessage);
			  } else {
			  $pushAns = $push->android($servicio->user->uuid, $pushMessage);
			  } */
			$servicio = Service::find($id);
			$push = Push::make();
			if($servicio->user->uuid == ''){
				return Response::json(array('error' => 0));
			}
			if($servicio->user->type == '1'){ //iPhone
				$result = $push->ios($servicio->user->uuid, $pushMessage), 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
			}else{
				$result = $push->android2($servicio->user->uuid, $pushMessage), 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
			}
			return Response::json(array('error' => 0));
		} else {
			return Response::json(array('error' => 1));
		}
	} else {
		return Response::json(array('error' => 3));
	}
}
?>
```

Este puede ser encontrado en refactoring/old_code.php

Luego, el código propuesto luego de la refactorización:

```
<?php public 
/**
* post_confirm
*
* @author: fabiocasmar
*
* @description:
*  Function to control the post confirmation
*/
function post_confirm(){
    // Initialize variables and and find the service and the driver
    $id = Input::get('service_id');
    $driver_id = Input::get('driver_id');
    $servicio = Service::find($id);

    // Error validations returns
    if($servicio == NULL){
        return Response::json(array('error' => 3));
    }
    if($servicio->status_id == '6'){
        return Response::json(array('error' => 2));
    }
    if(!($servicio->driver_id == NULL && $servicio->status_id == '1')){
        return Response::json(array('error' => 1));
    }
    if($servicio->user->uuid == ''){
        return Response::json(array('error' => 0));
    }
    
    // Update service status, driver status and update service car.
    $driverTmp = Driver::find($driver_id);
    $servicio = Service::update($id, 
        array(  'driver_id' => $driver_id,
                'status_id'   => '2'
            ));
    Drive::update($driver_id, array('available' => '0'));
    Service::update($id, array('car_id' => $driverTmp->car_id));

    // Send push notificaction
    $pushMessage = 'TU servicio ha sido confirmado';
    $push = Push::make();
    if($servicio->user->type == '1'){
        $result = $push->ios($servicio->user->uuid, $pushMessage), 
                            1, 'honk.wav', 'Open', 
                                array('serviceId' => $servicio->id));
    }else{
        $result = $push->android2($servicio->user->uuid, $pushMessage),
                            1, 'honk.wav', 'Open', 
                                array('serviceId' => $servicio->id));
        
    }
    return Response::json(array('message' => $result));
}

?>
```
Este puede ser encontrado en refactoring/refactored_code.php

## 2.3 Respuestas Preguntas Teóricas 2 ##

### 2.3.1 Las malas prácticas de programación que en su criterio son evidenciadas en el código. ###
1. Comentarios con código viejo innecesario.
2. Al llamar a una función algunas veces se pasa como parámetro la ejecución de la función directamente.
3. Los nombres de las variables no usan la misma nomenclatura, debería usarse el estilo "studly caps".
4. Se cambia el status del servicio y conductor, para luego realizar una última verificación. Esta se debe realizar antes de hacer cualquier cambio.
5. La serie de ifs anidados hace el código un poco confuso.
6. Posee líneas de más de 80 caracteres, demasiado largo.
7. No posee comentarios que ayuden a entender el código.
8. El return final devuelve un error aunque se haya enviado la notificación Push.

### 2.3.2 Cómo su refactorización supera las malas prácticas de programación: ###
1. Se eliminaron los comentarios de código viejo e innecesario.
2. Se creo una variable a la cual se le asigno el resultado de la ejecución de la función, de esta forma evitamos llamar a la función multiples veces.
3. Se modificaron los ifs anidados, y se colocarlos como una serie de ifs continuos. Estos fueron colocados al principio de la función, de modo que se valide todo antes de realizar cualquier cambio al status.
4. Las líneas que poseen más de 80 caracteres se coloco en dos líneas.
5. Se agregaron comentarios que ayudan a entender el funcionamiento del código.
6. Cuando no da error, se retorna el resultado. 

# 3 PREGUNTAS (10 puntos) #
## 3.1 Enunciado ##
Responda y envíe en un documento las siguientes preguntas:

1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?
2. ¿Qué características tiene según su opinión “buen” código o código limpio?  
3. ¿Qué es un microservicio?, ventajas y desventajas de los micorservicios

## 3.2 Respuestas Preguntas Teóricas 3 ##

### 3.2.1 ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?   ###

Este principio postula que una clase debe tener una sóla razón para cambiar. Esto tiene que ver con el nivel de acoplamiento entre módulos del Software, y quiere decir que la responsabilidad de una funcionalidad del software esta encapsulada completamente por una clase.

### 3.2.2 ¿Qué características tiene según su opinión “buen” código o código limpio?  ###

1. Nomenclatura de variables bien identificado.
2. Código bien indentado.
3. Código con suficiente comentarios para entender el algoritmo.
4. La clases desarrolladas cumplen con el principio de responabilidad única.
5. No debe haber código repetido.
6. El código debe ser legible.
7. Código lo más corto posible.
8. Debe contener pruebas unitarias.

### 3.2.3 ¿Qué es un microservicio?, ventajas y desventajas de los micorservicios ###


Microservicio es un estilo de arquitectura de Software basada en un conjunto de pequeños servicios, cada uno corriendo en sus propios procesos y comunicándose mediante mecanismos livianos, generalmente usando API HTTP.

Los servicios son construidos alrededor de funcionalidades de negocio y desplegados de manera independiente a través de una maquinaria de despliegue completamente automatizada.

Lo servicios pueden ser escritos en diferentes lenguajes de programación y utilizar diferentes tecnologías de almacenamiento, ya que se comunican entre ellos con una capa intermedia como lo es un API HTTP. El conjunto de microservicios realizan el funcionamiento del software completo

En la siguiente imágen podemos ver una Aquitectura monolítica:

![Arquitectura Monolítica](./READMEimg/arq1.jpg)

En la siguiente imágen podemos ver una Aquitectura de microservicios:

![Arquitectura Microservicios](./READMEimg/arq2.jpg)


#### 3.2.3.2 Ventajas y Desventajas ####

Ventajas: 

1. El código es fácil de mantener ya que cada módulo es relativamente pequeño.
2. Cada servicio es independiente, por lo cual se puede actualiza facilmente.
3. Cada módulo es tan independiente que permite escalar mejor el desarrollo.
4. Ayuda a aislar las fallas de código.
5. Flexibilidad de tecnologías y leguajes de programación.
6. Permite escalar los módulo necesarios y no todo el software.

Desventajas:

1.  Desarrollo de un sistema distribuido agrega complejidad al desarrollador.
2. Las pruebas en un sistema distribudo puede ser más complicado.
3. Los mecanismos de comunicación entre los módulos deben ser implementados por el desarrollador.
4. Las transacciones distribuidas entre multiples servicios puede ser bastante compleja.
5. Si el sistema es reativamente pequeño los costos de despliege pueden ser altos, ya que generalmente cada servicio se ejecuta en una máquina virtual, y en un enfoque monolítico en una sola VM todo el sistema.

