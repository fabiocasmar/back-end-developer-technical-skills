<?php

    $_fp = fopen("php://stdin", "r");

    $T = fgets($_fp);


    $cube = [];

    if (1 <= $T && $T <= 50) {
        $message = "";
        for($it = 0; $it < $T; $it++) {
            $configLine = fgets($_fp);
            list($N, $M) = preg_split("/[\s]+/", $configLine);

            if ((1 <= $N && $N <= 100) && (1 <= $M && $M <= 1000)) {
                $cube = new cube($N);
                for($j = 0; $j < $M; $j++) {
                    $line = "";
                    $line = fgets($_fp);
                    echo $cube->command($line);
                }
            }
        }
    }


   /**
    * Cube
    *
    * @author: fabiocasmar
    *
    * @description:
    * class with business rule to operate the querys in a Cube, 
    *     3 dimensional matix
    * This class is implemented with Binary Indexed Tree
    */

    class Cube {

       /** 
        * Attributes
        * @description: 
        * The class contains two attributes, the value of the Cube implemented
        *    as Binary Indexed Tree and the Cube size.
        *
        * [$cube 3d]
        * @var [array[n][n][n]]
        */
        private $cube;
        /**
        * [$N]
        * @var [interger]
        */
        private $N;

      /**
       * [__construct the constructor create the variables cube and n, 
       *     initialize the value of n with N and call the createCube function]
       * @param [integer] $n [defines matrix]
       */
        public function __construct($N) {
            $this->cube = [];
            $this->n = $N;
            $this->createCube();
        }


       /**
        * [createsMatrix initialize the three dimensional matrix with value
        *     zero at each position.]
        */
        private function createCube(){
            for($x = 0; $x <= $this->n; $x++) {
                $this->cube[$x] = [];
                for($y = 0; $y <= $this->n; $y++) {
                    $this->cube[$x][$y] = [];
                    for($z = 0; $z <= $this->n; $z++) {
                        $this->cube[$x][$y][$z] = 0;
                    }
                }
            }
        }

       /**
        * [summatory it's an auxiliary function to know the value of a range]
        * @param  [type] $x
        * @param  [type] $y
        * @param  [type] $z
        * @return [integer]
        */
        private function summatory($x, $y, $z) {
            $y1=0;
            $x1=0;
            $sum=0;
            while ($z>0) {
                $x1=$x;
                while($x1>0) {
                    $y1=$y;
                    while($y1>0) {
                        $sum +=  $this->cube[$x1][$y1][$z];
                        $y1-= ($y1 & -$y1);   
                    }
                    $x1 -= ($x1 & -$x1);
                }
                $z -= ($z & -$z);

            }
            return $sum;
        }

      /**
       * [update updates a position of the cube with $value]
       * @param  [integer] $x
       * @param  [integer] $y
       * @param  [integer] $z
       * @param  [integer] $value
       */
        private function update($x, $y, $z, $val){
            while($z <= $this->n) {
                $auxX = $x;
                while($auxX <= $this->n ){
                    $auxY = $y;
                    while($auxY <= $this->n) {
                        $this->cube[$auxX][$auxY][$z] += $val;
                        $auxY += ($auxY & -$auxY );
                    }
                    $auxX += ($auxX & -$auxX);
                }
                $z += ($z & -$z);
            }
        }

       /**
        * [command execute the command, UPDATE or QUERY in the cube]
        * @param  [type] $line
        * @return [string]
        */
        public function command($line){
            $matches = preg_split("/[\s]+/", $line);
            if ($matches[0] == "UPDATE" ){
                list($type, $x, $y, $z, $value) = $matches;
                $auxX = $x;
                $auxY = $y;
                $auxZ = $z;
                $value +=  - $this->summatory($x,$y,$z) 
                            + $this->summatory($auxX-1,$y,$z)
                             + $this->summatory($x,$auxY-1,$z) 
                              - $this->summatory($auxX-1,$auxY-1,$z) 
                               + $this->summatory($x,$y,$auxZ-1) 
                                - $this->summatory($auxX-1,$y,$auxZ-1)
                                 - $this->summatory($x,$auxY-1,$auxZ-1) 
                                  + $this->summatory($auxX-1,$auxY-1,$auxZ-1);
                $this->update($x, $y, $z, $value);
            }elseif ($matches[0] == "QUERY"){
                list($type, $auxX, $auxY, $auxZ, $x, $y, $z) = $matches;
                $result =   $this->summatory($x,$y,$z) 
                            - $this->summatory($auxX-1,$y,$z) 
                             - $this->summatory($x,$auxY-1,$z)  
                              + $this->summatory($auxX-1,$auxY-1,$z)  
                               - $this->summatory($x,$y,$auxZ-1) 
                                + $this->summatory($auxX-1,$y,$auxZ-1) 
                                 + $this->summatory($x,$auxY-1,$auxZ-1) 
                                  - $this->summatory($auxX-1,$auxY-1,$auxZ-1) ;

                return "$result \n";
            }      
        }
    }

?>